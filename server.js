var	express = require('express'),
	web = express(),
	fs = require('fs'),
	jade = require('jade');

var bitbucket = require('./bitbucket_api');

web.configure(function () {
	web.use(express.bodyParser());
	web.use(express.methodOverride());
	web.use(web.router);
	web.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
	web.set('views', __dirname + '/views');
});

web.isBrowserRequest = function(req){
	if (!req.xhr && (req.accepts('json,html') == 'html')){
		return true;
	}
	return false;
};

web.jadeInclude = function(filename, localVars){
	var file = fs.readFileSync(__dirname + '/views/' + filename + '.jade');
	var j = jade.compile(file);
	return j(localVars || {});
};

// Routes

web.get('/user', function (req, res){
	var reply = {'status': 'OK', 'paths': ['/user/:name']};
	if(web.isBrowserRequest(req)){
		res.render('main.jade', {'title': 'API endpoint: /user', 'text': web.jadeInclude('user'), 'response': reply});
		res.end();
	} else {
		res.type('json');
		res.json(reply);
		res.end();
	}
});

web.get('/user/:name', function(req, res){
	console.log(req.param('name'));
	bitbucket.user(req.param('name'), function(err, repos){
		if (err){
			console.log(err);
			res.type('json');
			res.send(err);
			res.end();
		}
		if (web.isBrowserRequest(req)){
			res.render('main.jade', {'title': 'API endpoint: /user/username', 'text': web.jadeInclude('user.username', repos), 'response': repos});
			res.end();
		} else {
			res.type('json');
			res.json(repos);
			res.end();
		}
	});
});

web.all('/user/*', function(req, res){
	res.type('json');
	res.json({'error': {'message': 'That request type is not supported by this route.'}});
	res.end();
});

web.get('/repo', function (req, res){
	var reply = {'status': 'OK', 'paths': ['/repo/:user/repo']};
	if(web.isBrowserRequest(req)){
		res.render('main.jade', {'title': 'API endpoint: /repo', 'text': web.jadeInclude('repo'), 'response': reply});
		res.end();
	} else {
		res.type('json');
		res.json(reply);
		res.end();
	}
});

web.get('/repo/:user/:repo', function(req, res){
	var repoName = req.param('user') + '/' + req.param('repo');
	console.log(repoName);
	bitbucket.repo(repoName, function(err, repo){
		console.log(repo);
		if(web.isBrowserRequest(req)){
			res.render('main.jade', {'title': 'API endpoint: /repo/user/reponame', 'text': web.jadeInclude('repo.reponame', repo), 'response': repo});
			res.end();
		} else {
			res.type('json');
			res.json(repo);
			res.end();
		}
	});
});

web.get('/', function (req, res) {
	var reply = {'status': 'ok', 'paths': ['/user', '/repo']};
	if(web.isBrowserRequest(req)){
		res.render('main.jade', {'title': 'API endpoint: /', 'text': web.jadeInclude('root'), 'response': reply});
		res.end();
	} else {
		res.type('json');
		res.json(reply);
		res.end();
	}
});


web.listen(process.env.PORT || 4000);






