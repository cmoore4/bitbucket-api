BitBucket API
====

I wanted to add some information to my blog about my recent activity on BitBucket, but the site provides no API, so I hacked together a web scraper in node to get repository names and recent commit information.

This is a self-documenting API, all urls should return information about what can be dome from them.  Additionally, when API endoints are accessed from a browser that is requesting HTML, it returns HTML pages that may include more readable information as well as the original json response.

Current useful endpoints are:

*   /repo/USER/REPONAME
*   /user/USERNAME

I'd like to add paginated requests for commit pages, such as /repo/user/repo/commits/1,2,3.  Also, maybe a full tree-view of teh source code as a single object.  Maybe.

A demo site is up at [http://bitbucket.csmooreinc.com/](http://bitbucket.csmooreinc.com/)
