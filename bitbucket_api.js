var htmlParser = require('cheerio'),
	request = require('request'),
	async = require('async');

var bitbucket = {
	'user': function (username, cb){

		var reposFetched = 0;
		var user = {
			'count': 0,
			'repos': [],
			'error': false
		};
		var done = false;
		var err = null;

		var userPage = 'https://bitbucket.org/' + username;

		function getUser(err, resp, html){
			if (err) return cb.call(undefined, {'message': err.toString()}, null);
			if (resp.statusCode != 200) return cb.call(undefined,{'message': 'Bitbucket returned 404, does this user exist?'}, null);
			var $ = htmlParser.load(html);

			// Collect user information
			user.name = $('#user-profile h1').text().trim();
			
			$('.user-info li').map(function(i, el){
				var $el = $(el);
				if($el.text().substr("Member since") !== -1){
					user.joined = $el.find('time').text().trim();
				} else if ($el.text().substr('http:') !== -1){
					user.site = $el.text().trim();
					console.log('Got site' + $el.text().trim());
				} else {
					// Has to be location, since bb only allows 3 fields
					user.location = $el.text().trim();
				}
			});

			// Collect repository names and urls
			$('.repo-filter-widget .filter-item').map(function(i, el){
				var $el = htmlParser(el);
				var repo = {
					'name': $el.text().trim(),
					'url': 'https://bitbucket.org' + $el.find('a').attr('href')
				};
				user.repos.push(repo);
			});

			// User exists, but no public repos
			if(user.repos.length === 0){
				user.count = '0';
				cb.call(undefined, err, user);
			}

			// Get all the repos via async request
			async.forEach(user.repos, getRepo, function(err){
				if (err) return cb.call(undefined, {'message': err.toString()}, null);
			});
		}

		function getRepo(repo){
			request(repo.url, function(err, resp, html){
				// TODO: Error handling per repo
				repo.recentCommits = [];
				var $ = htmlParser.load(html);
				$('.changeset').map(function(i, el){
					repo.recentCommits.push($(el).text().trim().replace(/\s+/gi, ' '));
				});
				reposFetched += 1;
				if (reposFetched == user.repos.length){
					user.count = user.repos.length;
					cb.call(undefined, err, user);
				}
			});
		}

		request(userPage, getUser);
	},

	'repo': function(repo, cb){
		function getRepo(err, resp, html){
			if (err) return cb.call(undefined, {'message': err.toString()}, null);
			if (resp.statusCode != 200) return cb.call(undefined, {'error': 'Bitbucket returned 404, does this user exist?'}, null);
			var $ = htmlParser.load(html);

			var repoResponse = {
				'readme' : {
					'html': null,
					'text': null
				},
				'name': null
			};

			repoResponse.name = $('a.repo-link').text();
			repoResponse.readme.html = $('section#readme').html();
			repoResponse.readme.text = $('section#readme').text();

			return cb.call(undefined, null, repoResponse);
		}

		var repoUrl = 'https://bitbucket.org/' + repo;
		request(repoUrl, getRepo);
	}
};

module.exports = bitbucket;

